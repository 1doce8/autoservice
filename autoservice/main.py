import pymysql
import configparser
from PyQt5 import QtCore, QtGui, QtWidgets
import sys
import datetime
from ui import Ui_MainWindow

if __name__ == "__main__":
    # create app
    app = QtWidgets.QApplication(sys.argv)

    # create form and init ui
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()


# Hook logic
def db_connect():
    config = configparser.ConfigParser()
    config.read("settings.ini")
    try:
        connection = pymysql.connect(host=config["MYSQL"]["host"],
                                     user=config["MYSQL"]["user"],
                                     password=config["MYSQL"]["password"],
                                     db=config["MYSQL"]["dataBase"],
                                     autocommit=True)

    except Exception:
        connected = False
    else:
        connected = True
    finally:
        return connection, connected

def insert_new(master, client, repair):
    try:
        sql = "INSERT INTO orders(id_master, id_client, id_repair_type) VALUES('" + master + "', '" + client + "', '" + repair + "')"
        execute_sql(sql)

    except Exception:
        result = False

    else:
        result = True

    finally:
        return result

def execute_sql(sql):
    connection, connected = db_connect()
    if connected:
        with connection.cursor() as cursor:
            try:
                cursor.execute(sql)
                result = cursor.fetchall()
                return result

            except Exception:
                result = False

            else:
                result = True

            finally:
                cursor.close()
                return result

def truncate_table():
    sql = "TRUNCATE orders"
    res = execute_sql(sql)
    fill_table_all()
    message_box('success', 'Table successfully truncate')

if __name__ == "__main__":

    def message_box(title, message):
        mess = QtWidgets.QMessageBox()
        mess.setWindowTitle(title)
        mess.setText(message)
        mess.setStandardButtons(QtWidgets.QMessageBox.Ok)
        mess.exec_()

    def add_new():
        master = str(ui.master_selector.currentIndex()+1)
        print(master)
        client = str(ui.client_selector.currentIndex()+1)
        repair = str(ui.repair_selector.currentIndex()+1)
        insert_new(master, client, repair)
        fill_selectors()
        fill_table_all()
        message_box('success', 'Задание оформлено')

    # def get_interval():
    #     date_start = ui.date_start.text()
    #     date_end = ui.date_end.text()
    #     sql = "SELECT * FROM pp WHERE time BETWEEN '" + date_start + "' AND '" + date_end + "'"
    #     result = execute_sql(sql)
    #     ui.tableWidget.setRowCount(0)
    #
    #     for row_number, row_data in enumerate(result):
    #         ui.tableWidget.insertRow(row_number)
    #         for column_number, data in enumerate(row_data):
    #             ui.tableWidget.setItem(row_number, column_number, QtWidgets.QTableWidgetItem(str(data)))


    def get_masters():
        sql = "SELECT name, surname FROM masters"
        data = execute_sql(sql)
        ui.master_selector.clear()
        ui.master_selector_view.clear()
        comboBox = [row[0] + ' ' + row[1] for row in data]
        for row in comboBox:
            ui.master_selector.addItem(row)
            ui.master_selector_view.addItem(row)

    def get_clients():
        sql = "SELECT name, surname FROM clients"
        data = execute_sql(sql)
        ui.client_selector.clear()
        comboBox = [row[0] + ' ' + row[1] for row in data]
        for row in comboBox:
            ui.client_selector.addItem(row)

    def get_repair():
        sql = "SELECT name FROM repair_type"
        data = execute_sql(sql)
        ui.repair_selector.clear()
        comboBox = [row[0] for row in data]
        for row in comboBox:
            ui.repair_selector.addItem(row)


    def fill_selectors():
        get_masters()
        get_clients()
        get_repair()

    def fill_table(sql):
        result = execute_sql(sql)
        ui.tableWidget.setRowCount(0)
        for row_number, row_data in enumerate(result):
            ui.tableWidget.insertRow(row_number)
            for column_number, data in enumerate(row_data):
                ui.tableWidget.setItem(row_number, column_number, QtWidgets.QTableWidgetItem(str(data)))

    def fill_table_all():
        sql = "SELECT orders.id, masters.name as masterName, clients.name, clients.car, repair_type.name " \
              "FROM orders " \
              "LEFT JOIN masters ON orders.id_master = masters.id " \
              "LEFT JOIN repair_type ON orders.id_repair_type = repair_type.id " \
              "LEFT JOIN clients ON orders.id_client= clients.id " \
              "ORDER BY id ASC"
        fill_table(sql)

    def fill_table_master():
        sql = "SELECT orders.id, masters.name as masterName, clients.name, clients.car, repair_type.name " \
              "FROM orders " \
              "LEFT JOIN masters ON orders.id_master = masters.id " \
              "LEFT JOIN repair_type ON orders.id_repair_type = repair_type.id " \
              "LEFT JOIN clients ON orders.id_client = clients.id " \
              "WHERE orders.id_master = "+str(ui.master_selector_view.currentIndex()+1)+" " \
              "ORDER BY id ASC "
        print(sql)
        fill_table(sql)
        #message_box('success', 'Данные загружены')



    fill_table_all()
    fill_selectors()

    ui.add.clicked.connect(add_new)
    ui.load_all_btn.clicked.connect(fill_table_all)

    ui.clear_database_btn.clicked.connect(truncate_table)

    ui.load_master_orders.clicked.connect(fill_table_master)

    #ui.state.clear()
    #ui.state.insertItem(0, 'False')
    #ui.state.insertItem(1, 'True')

    # Run main loop
    sys.exit(app.exec_())
