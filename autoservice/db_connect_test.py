import sys
import pymysql
import configparser
import unittest


class ProgramTestCase(unittest.TestCase):
    """ Тест для main.py """

    def test_insert_into_table(self):

        from main import db_connect

        connection, connected = db_connect()
        self.assertEqual(connected, True)


if __name__ == "__main__":
    unittest.main()
